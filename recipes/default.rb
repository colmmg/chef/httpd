package 'httpd' do
  action :install
end

file '/var/www/html/index.html' do
  content 'Hello World!'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end